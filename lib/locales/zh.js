i18n.translations.zh = {
  //Navigation
  "View": "排列",
  "Menu": "菜單",
  "Top": "置頂",
  "New": "最新",
  "Digest": "日期",
  "Categories": "分類",
  "Users": "用戶",
  "Settings": "設置",
  "Admin": "管理",
  "Post": "提交",
  "Toolbox": "工具箱",
  "Sign Up/Sign In": "注冊/登錄",
  "My Account": "帳號",
  "View Profile": "個人資料",
  "Edit Account": "編輯帳號",

  //Main
  "New Posts": "最新提交",

  //Commments
  "Your comment has been deleted.": "你的評論已經被刪除",
  "Comment": "評論",
  "Delete Comment": "刪除評論",
  "Add Comment": "評論",
  "upvote": "頂",
  "downvote": "踩",
  "link": "鏈接",
  "Edit": "編輯",
  "Reply": "回復",
  "No comments.": "暫時沒有評論",

  //Errors
  "You are already logged in": "你已經登錄",
  "Sorry, this is a private site. Please sign up first.": "對不起, 請先注冊再進行後續操作",
  "Thanks for signing up!": "感謝您的注冊!",
  "The site is currently invite-only, but we will let you know as soon as a spot opens up.": "該站點暫時只允許邀請訪問, 如果開放了我們會讓你知道",
  "Sorry, you don't have the rights to view this page.": "抱歉你沒有權利查看此頁面",
  "Not Found!": "頁面不存在",
  "We're sorry; whatever you were looking for isn't here..": "抱歉沒有你要查看的內容!",

  //Notifications
  "No notifications": "無消息",
  "1 notification": "1 個消息",
  "notifications": "消息",
  "Mark all as read": "標記所有",

  // Post deleted
  "Your post has been deleted.": "你的帖子已經被刪除",

  // Post digest
  "The top 5 posts of each day.": "每天前5名的帖子",
  "Previous Day": "前一天",
  "Next Day": "後一天",
  "Sorry, no posts for today": "抱歉今天沒有新的帖子",
  "Today": "今天",
  "Yesterday": "昨天",

  // Post submit & edit
  "Created": "創建",
  "Title": "標題",
  "Suggest title": "顯示標題",
  "URL": "鏈接地址",
  "Short URL": "短網址",
  "Body": "內容",
  "Category": "分類",
  "Inactive?": "Inactive?",
  "Sticky?": "置頂?",
  "Submission Date": "提交日期",
  "Submission Time": "提交時間",
  "Date": "日期",
  "Submission": "提交",
  "Note: this post is still pending so it has no submission timestamp yet.": "這篇文章沒有進行審核.",
  "User": "用戶",
  "Status": "專題",
  "Approved": "審核",
  "Rejected": "拒接",
  "Delete Post": "刪除帖子",
  "Thanks, your post is awaiting approval.": "感謝, 你的帖子正在等待批准.",
  "Sorry, couldn't find a title...": "抱歉找不相關話題",
  "Please fill in an URL first!": "請在第一欄填寫鏈接",

  // Post item
  "Share": "分享",
  "Discuss": "討論",
  "Upvote": "頂",
  "Sticky": "置頂",
  "status": "狀態",
  "votes": "得票",
  "baseScore": "基礎得分",
  "score": "得分",
  "clicks": "點擊",
  "inactive": "不活躍",
  "comment": "評論",
  "comments": "評論數",
  "point": "點擊",
  "points": "點擊數",

  //User
  "Please complete your profile below before continuing.": "在繼續之前請填寫相關資料.",
  "Account": "帳號",
  "Username": "用戶名",
  "Display Name": "暱稱",
  "Email": "Email",
  "Bio": "自我介紹",
  "Password": "密碼",
  "Change Password?": "修改密碼?",
  "Old Password": "舊密碼",
  "New Password": "新密碼",
  "Email Notifications": "郵箱提醒",
  "New Posts": "最新主題",
  "Comments on my posts": "評論我的主題時",
  "Replies to my comments": "回復我的回復時",
  "Forgot password?": "忘記密碼?",
  "Profile updated": "更新資料",
  "Please fill in your email below to finish signing up.": "請填寫你的電子郵件完成注冊.",
  "Invite": "邀請",
  "Uninvite": "未激活",
  "Make admin": "設置為管理員",
  "Unadmin": "取消管理資格",
  "Delete User": "刪除用戶",
  "Are you sure you want to delete ": "你確定要刪除用戶嗎 ",
  "Reset Password": "重置密碼",
  "Password reset link sent!": "密碼重置鏈接已發送",
  "Name": "名字",
  "Posts": "帖子數",
  "Comments": "評論數",
  "Karma": "Karma",
  "Is Invited?": "邀請用戶?",
  "Is Admin?": "管理員?",
  "Delete": "刪除",
  "Member since": "加入至今",
  "Edit profile": "修改個人資料",
  "Sign In": "登錄",
  "Sign in!": "登錄!",
  "Sign up!": "注冊!",
  "Don't have an account?": "還沒有帳號?",
  "Already have an account?": "已有帳號?",
  "Sign Up": "注冊",
  "Please fill in all fields": "請填寫完整",
  "Invite ": "邀請 ",
  "left": " restante",
  "Invite (none left)": "邀請數 (無)",
  "All": "全部",
  "Invited": "邀請",
  "Uninvited": "未被邀請",
  "Filter by": "過濾",
  "Sort by": "排序",

  //helpers
  "Sorry, you do not have access to this page": "抱歉你沒有權限訪問此頁面",
  "Please Sign In First.": "請先登錄.",
  "Sorry, you  have to be an admin to view this page.": "抱歉你必須是管理員才能查看此頁面",
  "Sorry, you don't have permissions to add new items.": "抱歉你沒有權限添加新項.",
  "Sorry, you cannot edit this post.": "對不起你不能編輯這個帖子",
  "Sorry, you cannot edit this comment.": "對不起你不能編輯這個評論",

  //Collections
  "You need to login and be an admin to add a new category.": "你必須登錄並且是管理員才能添加新類別.",
  "You need to login or be invited to post new comments.": "你需要登錄或被邀請才能發表新的評論.",
  "Please wait ": "請稍等 ",
  " seconds before commenting again": " 秒後在評論",
  "Your comment is empty.": "你的評論是空的.",
  "You don't have permission to delete this comment.": "你沒有刪除此評論的權限.",
  "You need to login or be invited to post new stories.": "你需要登錄或被邀請才能發布新的內容.",
  "Please fill in a headline": "請填寫一個標題",
  "This link has already been posted": "這個鏈接已發布",
  "Sorry, you cannot submit more than ": "對不起, 內容不能超過",
  " posts per day": " 評價每日發帖",
  "Someone replied to your comment on": "有人回復了你的評論",
  " has replied to your comment on": " 已經有人回復了你的評論",
  "Read more": "查看更多",
  "A new comment on your post": "你發表的主題有新的評論",
  "You have a new comment by ": "你有一個新的評論在 ",
  " on your post": " 在你的帖子",
  " has created a new post": " 發一個新帖",
  "Your account has been approved.": "你的帳號已被批准",
  "Welcome to ": "歡迎來到 ",
  "Start posting.": "開始發布.",

  //Common
  "Pending": "懸而未決...",
  "Loading...": "加載中...",
  "Submit": "提交",
  "You must be logged in.": "你必須登錄.",
  "Are you sure?": "是否確定?",
  "Please log in first": "請先登錄",
  "Sign In/Sign Up with Twitter": "使用Twitter/注冊",
  "Load more": "加載更多",

  "Administration": "管理",
  "Best": "精華"
};

